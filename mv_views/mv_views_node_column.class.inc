<?php
/**
 * @file
 * This file contains the MVViewsNodeColumn class
 */
class MVViewsNodeColumn extends MVColumn {

    protected $table_name;
    protected $column_name;

    public function __construct($table_name, $column_name) {
        $this->table_name = $table_name;
        $this->column_name = $column_name;
    }

    public function getValue($entity_type, $entity_id) {
        $node = (array) MVEntityCache::get('node', $entity_id);

        if (isset($node->{$this->column_name})) {
            return $node->{$this->column_name};
        }

        // TODO: Probably stop assuming this is a CCK field.

        $values = array();
        //$column = substr($this->column_name, 0, -6);
        $column = $this->column_name;
        if (is_array($node[$column])) {
            foreach ($node[$column] as $value) {
                $values[] = $value['value'];
            }
        }

        if (count($values) == 1) {
            return array_pop($values);
        }

        return $values;
    }

    // Perform a direct node-to-node change mapping.
    public function getChangeMapping($entity_type, $entity_id) {
        $changed = array();
        if ($entity_type == 'node') {
            $changed[$entity_type] = array($entity_id);
        }
        return $changed;
    }

    public function getSchema() {
        $table = 'node';

        if ($this->table_name != 'node') {
            $views_path = drupal_get_path('module', 'views');
            require_once $views_path . '/includes/base.inc';
            require_once $views_path . '/includes/handlers.inc';
            //$table = views_get_table_join($this->table_name, 'node');
            //$table = $table->table;
        }

        $schema = drupal_get_schema('node');

        //$schema = drupal_get_schema_unprocessed('node', 'node');
        $field_schema = $schema['fields'][$this->column_name];

        // Override type "serial"
        if ($field_schema['type'] == 'serial') {
            $field_schema['type'] = 'int';
        }
        $field_schema['default'] = 0;
        $field_schema['not null'] = FALSE;

        return $field_schema;
    }

    public function getName() {
        return $this->column_name;
    }
}
