<?php
/**
 * @file
 * Defines an operator for use in static and dynamic filters.
 *
 * Subclass to define usable operators.
 */
abstract class MVOperator {
  /**
   * Applies the operator to $value.
   *
   * @param $value
   *   The value to be compared.
   *
   * @returns boolean
   */
  abstract public function apply($value);

  /**
   * Determines whether the operator would end the
   * usefulness of an index if added to the index.
   *
   * @returns boolean
   */
  abstract public function endsIndex();

  /**
   * Determines whether the operator is indexable.
   *
   * @returns boolean
   */
  abstract public function indexable();
}