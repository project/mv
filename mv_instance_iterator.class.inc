<?php
/**
 * @file
 * This class handles iterations through multi-valued dynamic filter values. Internal use only.
 */
final class MVInstanceIterator {
  protected $instance_tracker;
  protected $all_values;
  protected $done;

  public function __construct($all_values) {
    $this->done = FALSE;
    $this->instance_tracker = array();
    $this->all_values = array();
    foreach ($all_values as $key => $value) {
      if (is_array($value)) {
        $this->all_values[$key] = $value;
      }
      else {
        $this->all_values[$key] = array($value);
      }
      $this->instance_tracker[$key] = 0;
    }
  }

  public function getNextInstance() {
    $instance = $this->getCurrentInstance();
    $this->incrementInstance();
    return $instance;
  }

  protected function incrementInstance() {
    foreach ($this->instance_tracker as $key => $value) {
      if ($value < count($this->all_values[$key])) {
        ++$this->instance_tracker[$key];
        if ($this->instance_tracker[$key] == count($this->all_values[$key])) {
          $this->instance_tracker[$key] = 0;
        }
        else {
          return;
        }
      }
    }

    $this->done = TRUE;
  }

  protected function getCurrentInstance() {
    if ($this->done) {
      return FALSE;
    }

    $return = array();
    foreach ($this->instance_tracker as $key => $value) {
      $return[$key] = $this->all_values[$key][$value];
    }
    return $return;
  }
}