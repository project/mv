Materialized Views
After Summer of Code Project:
== INTEGRATION WITH ENTITY AND FIELD COLUMN ==
 * Supports Entities
 * Supports Fields

== INTEGRATION WITH VIEWS ==
 * Supports basic integration with views
 * Supports Filters, Sorts

== SHORTCOMINGS ==
 * Doesnot support multi-valued fields
 * Support of Views limited to Entity tables
 * Support of Views limited to columns having same entities. Views consisting of various entities not supported
 * Doesnot support various features of views like Pager


Running this pre-release:
* Install all MV-related modules.
* Go to admin/reports/materialized_view
* Click "Reconcile now"
