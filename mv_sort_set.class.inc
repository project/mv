<?php
/**
 *
 * @file
 * Defines a sort set that corresponds to ORDER BY criteria to be used on the table.
 *
 * Add to MVs using $mv->addSortSet($my_sort_set);
 */
class MVSortSet {
  protected $sorts = array();
  protected $name;
  protected $id;
  protected static $max_id = 0;

  public function __construct($name = NULL) {
    $this->id = self::$max_id++;
    $this->name = $name;
  }

  public function getName() {
    if (isset($this->name)) {
      return $this->name;
    }
    else {
      return 'mv_sort_set_' . $this->id;
    }
  }

  /**
   * Adds sort column configured to run at query time.
   *
   * @param $column
   *   MVColumn object to be used for sorting.
   * @param $direction
   *   Constant defining sort order. Use MV_SORT_ASCENDING
   *   or MV_SORT_DESCENDING.
   */
  public function addSort(MVColumn $column, $direction) {
    $sort = new stdClass();
    $sort->column = $column;
    $sort->direction = $direction;
    $this->sorts[] = $sort;
  }

  /**
   * Gets the an array of the sorts in the set.
   *
   * @returns
   *   An array of the sorts.
   */
  public function getSorts() {
    return $this->sorts;
  }
}