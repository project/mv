<?php

/**
 *
 * @file
 *
 * This file contains the main Materialized Views class.
 *
 */
define('MV_SORT_ASCENDING', 'ASC');
define('MV_SORT_DESCENDING', 'DEC');

/**
 * Master class used for describing materialized views. Usually returned from hook_materialized_view_info().
 */
class MaterializedView {

  protected $dynamic_filters = array();
  protected $static_filters = array();
  protected $extra_columns = array();
  protected $sort_sets = array();
  protected $name;

  /**
   * Constructs a MaterializedView object.
   *
   * @param $name
   *   The name of the materialized view.
   */
  public function __construct($name) {
    $this->name = $name;
  }

  /**
   * Gets the name of the materialized view.
   *
   * @returns
   *   The name of the materialized view.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Adds extra column that provides data consolidation but
   * is not indexed.
   *
   * @param $column
   *   MVColumn object to be used as the data source.
   */
  public function addExtraColumn(MVColumn $column) {
    $this->extra_columns[] = $column;
  }

  /**
   * Adds filter with a predefined test that runs when objects are updated.
   *
   * @param $column
   *   MVColumn object to be used for sorting.
   * @param $operation
   *   The operation to be used on this column.
   * @param $value
   *   The value or values to be used for the operation.
   */
  public function addStaticFilter(MVColumn $column, MVOperator $operator) {
    $filter = new stdClass();
    $filter->column = $column;
    $filter->operator = $operator;
    $this->static_filters[] = $filter;
  }

  /**
   * Adds filter configured to run at query time.
   *
   * @param $column
   *   MVColumn object to be used for sorting.
   * @param $operation
   *   The operation to be used on this column.
   */
  public function addDynamicFilter(MVColumn $column, MVOperator $operator) {
    $filter = new stdClass();
    $filter->column = $column;
    $filter->operator = $operator;
    $this->dynamic_filters[] = $filter;
  }

  /**
   * Adds a sort set.
   *
   * @param $sort_set
   *   MVSortSet object to be used as a sort set.
   */
  public function addSortSet(MVSortSet $sort_set) {
    $this->sort_sets[] = $sort_set;
  }

  /**
   * Gets the an array of the dynamic filters in the set.
   *
   * @returns
   *   An array of the filters.
   */
  public function getDynamicFilters() {
    return $this->dynamic_filters;
  }

  /**
   * Get an array listing columns to be indexed.
   */
  protected function getIndexes() {
    $indexes = array();
    $base_index = array();

    // TODO OPTIMIZATION: Sort columns in this order:
    // (1) Indexable conditions that don't end the index.
    // (2) Indexable conditions that end the index.
    // (3) Unindexable conditions.

    foreach ($this->dynamic_filters as $dynamic_filter) {
      $base_index[] = $dynamic_filter->column->getName();

      // TODO OPTIMIZATION: Stop building this index if we encounter a filter that ends the index.
    }

    if (empty($this->sort_sets)) {
      if (!empty($base_index)) {
        $indexes['mv_base'] = $base_index;
      }
    }
    else {
      foreach ($this->sort_sets as $sort_set) {
        $index = $base_index;
        $sorts = $sort_set->getSorts();

        // TODO OPTIMIZATION: Apply effective order reversals to maximize ASC/DESC consistency.

        foreach ($sort_set->getSorts() as $sort) {
          $index[] = $sort->column->getName();

          // TODO OPTIMIZATION: Stop building this index if the ASC/DESC direction changes.
        }
        $indexes[$sort_set->getName()] = $index;
      }
    }

    // TODO OPTIMIZATION: Prune indexes that are proper prefixes of other indexes.

    return $indexes;
  }

  /**
   * Gets the an array of the sort sets.
   *
   * @returns
   *   An array of the sort sets.
   */
  public function getSortSets() {
    return $this->sort_sets;
  }

  /**
   * Build the schema for the materialized view table.
   */
  public function getSchema() {
    $schema = array();

    $schema['fields'] = array(
        'entity_type' => array(
            'type' => 'varchar',
            'length' => 64,
            'not null' => TRUE,
            'description' => 'The entity type.',
        ),
        'entity_id' => array(
            'type' => 'int',
            'not null' => TRUE,
            'default' => 0,
            'unsigned' => TRUE,
            'description' => 'The entity ID.',
        ),
    );

    // Add dynamic filter columns
    foreach ($this->dynamic_filters as $dynamic_filter) {
      $schema['fields'][$dynamic_filter->column->getName()] = $dynamic_filter->column->getSchema();
    }

    // Add sort columns
    foreach ($this->sort_sets as $sort_set) {
      foreach ($sort_set->getSorts() as $sort) {
        $schema['fields'][$sort->column->getName()] = $sort->column->getSchema();
      }
    }

    // Add extra columns
    foreach ($this->extra_columns as $extra_column) {
      $schema['fields'][$extra_column->getName()] = $extra_column->getSchema();
    }

    $schema['description'] = 'Materialized view storage for ' . $this->name . '.';
    $schema['indexes'] = $this->getIndexes();
    $schema['primary key'] = $this->getPrimaryKey();

    return $schema;
  }

  /**
   * Install the schema for this materialized view.
   */
  public function installSchema() {
    // TODO: Remove the ability to trample existing, non-MV tables.
    // First, remove all data for this MV.
    $this->uninstallSchema();

    // Generate and install the schema.
    $ret = array();
    $schema = $this->getSchema();
    db_create_table($this->name, $schema);

    // Rebuild the schema cache.
    drupal_get_schema(NULL, TRUE);

    // Add the schema hash to the database.
    $schema_hash = sha1(serialize($schema));
    //db_query('INSERT INTO {materialized_view} (schema_hash, mvid) VALUES ("%s", "%s")', $schema_hash, $this->getName());

    db_insert('materialized_view')
            ->fields(array(
                'schema_hash' => $schema_hash,
                'mvid' => $this->getName(),
            ));

    watchdog('mv', 'Installed: ' . check_plain($this->getName()));
  }

  /**
   * Uninstall the schema for this materialized view.
   */
  public function uninstallSchema() {
    $ret = array();
    if (db_table_exists($this->name)) {
      db_drop_table($this->name);
    }

    // Clear any record of the installed schema.
    db_query('DELETE FROM {materialized_view} WHERE mvid = :mvid', array(':mvid' => $this->getName()));

    // Reset all indexing by clearing all rows in the index tracker for this MV.
    db_query('DELETE FROM {materialized_view_indexing} WHERE mvid = :mvid', array(':mvid' => $this->getName()));

    watchdog('mv', 'Uninstalled: ' . check_plain($this->getName()));
  }

  /**
   * Verify that the installed schema is current. If not, update it.
   */
  public function verifySchema() {
    $effect = FALSE;

    // Load the mview record from the database.
    $installed_schema_hash = db_query('SELECT schema_hash FROM {materialized_view} WHERE mvid = :mvid', array(':mvid' => $this->getName()))->fetchField();

    // Check that any schema for this MV are installed.
    if ($installed_schema_hash) {
      $schema_hash = sha1(serialize($this->getSchema()));
      // Check that schema are present and up-to-date.
      if ($schema_hash != $installed_schema_hash) {
        // On mismatch, (re)install the schema.
        $this->installSchema();
        $effect = TRUE;
      }
    }
    else {
      // Install the schema for the first time.
      $this->installSchema();
      $effect = TRUE;
    }

    return $effect;
  }

  /**
   * Index up to $batch_size items from each entity type.
   */
  public function index($batch_size) {
    $last_id = NULL;
    $existing_cursor = NULL;

    // Get a list of all entity types.
    $entity_types = materialized_view_entity_type_get();

    // Get indexing status for all entity types.
    $index_status = array();
    $res = db_query('SELECT entity_type, max_indexed_id FROM {materialized_view_indexing} WHERE mvid = :mvid', array(':mvid' => $this->getName()));
    foreach ($res as $row) {
      if (array_key_exists($row->entity_type, $entity_types)) {
        // Store the current indexing pointer with the entity type.
        $entity_types[$row->entity_type]['max_indexed_id'] = $row->max_indexed_id;
      }
      else {
        // Delete the indexing cursor if the entity type no longer exists.
        db_query('DELETE FROM {materialized_view_indexing} WHERE mvid = :mvid AND entity_type = :entity_type', array(':mvid' => $this->getName(), ':entity_type' => $row->entity_type));
      }
    }

    // Index a batch from each entity type.
    foreach ($entity_types as $entity_type_name => $entity_type_info) {
      // If there's no record of the maximum ID indexed, use one more than the current maximum.
      $existing_cursor = TRUE;
      if (!array_key_exists('max_indexed_id', $entity_type_info)) {
        $max_id_sql = 'SELECT MAX(' . db_escape_table($entity_type_info['entity keys']['id']) . ') FROM {' . db_escape_table($entity_type_info['base table']) . '}';
        $entity_type_info['max_indexed_id'] = db_query($max_id_sql)->fetchField() + 1;
        $existing_cursor = FALSE;
      }

      if ($entity_type_info['max_indexed_id'] > 1) {
        // Fetch a batch of IDs to index.
        $id_sql = 'SELECT t.' . db_escape_table($entity_type_info['entity keys']['id']) . ' AS id FROM {' . db_escape_table($entity_type_info['base table']) . '} t WHERE t.' . db_escape_table($entity_type_info['entity keys']['id']) . ' < :id_column ORDER BY t.' . db_escape_table($entity_type_info['entity keys']['id']) . ' DESC LIMIT 0,' . $batch_size;

        $res = db_query($id_sql, array(
            ':id_column' => $entity_type_info['max_indexed_id'],
        ));

        // TODO: Make i18n.
        $log_entry = 'Indexing "' . check_plain($entity_type_name) . '" with IDs under ' . check_plain($entity_type_info['max_indexed_id']) . '.';
        watchdog('mv', $log_entry);

        // Index the IDs.
        while ($row = $res->fetchAssoc()) {
          $this->update($entity_type_name, $row['id']);
          $last_id = $row['id'];
        }

        // Update the indexing cursor.
        if ($existing_cursor) {
          db_update('materialized_view_indexing')
                  ->fields(array(
                      'max_indexed_id' => $last_id,
                  ))
                  ->condition(array(
                      'mvid' => $this->getName(),
                      'entity_type' => $entity_type_name,
                  ))
                  ->execute();
        }
        else {

          db_insert('materialized_view_indexing')
                  ->fields(array(
                      'max_indexed_id' => $last_id,
                      'mvid' => $this->getName(),
                      'entity_type' => $entity_type_name,
                  ))
                  ->execute();
        }
      }
    }
  }

  /**
   * Map changes using the active data sources and call updateMapped() to apply
   * each mapped change.
   *
   * @param $entity_type.
   *   The entity type.
   * @param $entity_id.
   *   The entity ID.
   */
  public function update($entity_type, $entity_id, $deletion_mode = FALSE) {
    $mappings = array();

    // Allow each source to map the change to affected entity type/ID pairs.
    foreach ($this->dynamic_filters as $source) {
      $mappings = array_merge_recursive($mappings, $source->column->getChangeMapping($entity_type, $entity_id));
    }
    foreach ($this->static_filters as $source) {
      $mappings = array_merge_recursive($mappings, $source->column->getChangeMapping($entity_type, $entity_id));
    }
    foreach ($this->extra_columns as $source) {
      $mappings = array_merge_recursive($mappings, $source->getChangeMapping($entity_type, $entity_id));
    }

    // TODO OPTIMIZATION: Only update the columns related to the change mappings.
    // In deletion mode, remove the actual deleted entity type/ID
    // (if present) from the update set.
    if ($deletion_mode && isset($mappings[$entity_type])) {
      $key = array_search($entity_id, $mappings[$entity_type]);
      if ($key !== FALSE) {
        unset($mappings[$entity_type][$key]);
      }
    }

    // For each unique entity type and ID, run the update.
    foreach ($mappings as $entity_type => $entity_ids) {
      foreach (array_unique($entity_ids) as $entity_id) {
        $this->updateMapped($entity_type, $entity_id);
      }
    }
  }

  /**
   * Update an object in this materialized view or remove it
   * if it no longer meets criteria for inclusion.
   *
   * @param $entity_type.
   *   The entity type.
   * @param $entity_id.
   *   The entity ID.
   *
   * @returns A boolean value indicating whether the entity passed static filtering.
   */
  protected function updateMapped($entity_type, $entity_id) {
    // TODO: Start a transaction.
    // TODO OPTIMIZATION: Intelligently update rows instead of doing a DELETE+INSERT.
    // Delete the existing rows related to the object in the database.
    $this->delete($entity_type, $entity_id, FALSE);

    // Loop through filters to determine if the updated object qualifies for inclusion.
    foreach ($this->static_filters as $static_filter) {

      // Get the value of the column.
      $value = $static_filter->column->getValue($entity_type, $entity_id);

      // Apply the operator
      if (!$static_filter->operator->apply($value)) {

        // Return if the entity does not meet static criteria.
        return FALSE;
      }
    }

    // The entity meets static critieria; add its row(s) to the MV table.
    // We always need to write the entity_type and entity_id to the row.
    $keys = $write = array(
        'entity_type' => $entity_type,
        'entity_id' => $entity_id,
    );

    // Get values for the extra columns
    foreach ($this->extra_columns as $extra_columns) {
      $write[$extra_columns->getName()] = $extra_columns->getValue($entity_type, $entity_id);
    }

    // Get values for the sort columns
    foreach ($this->sort_sets as $sort_set) {
      foreach ($sort_set->getSorts() as $sort) {
        // Sort colummns are often used multiple places, but we should only generate them up once.
        if (!array_key_exists($sort->column->getName(), $write)) {

          // TODO: Throw exception if a sort column value is an array.
          $write[$sort->column->getName()] = $sort->column->getValue($entity_type, $entity_id);
        }
      }
    }

    // Get values for the dynamic filter columns
    foreach ($this->dynamic_filters as $dynamic_filter) {
      $write[$dynamic_filter->column->getName()] = $dynamic_filter->column->getValue($entity_type, $entity_id);
    }

    // Construct an iterator to perform the cartesian join.
    $iterator = new MVInstanceIterator($write);

    // Store the instances using a cartesian join.
    while ($instance = $iterator->getNextInstance()) {
      // Write the record to the database
      $schema = drupal_get_schema($this->name);

      $success = drupal_write_record($this->name, $instance);
      if (!$success) {
        // TODO: Make translatable.
        watchdog('mv', 'Updating the materialized view "' . check_plain($this->getName()) . '" failed:<pre>' . check_plain(print_r($instance, TRUE)) . '</pre>');
      }
    }

    return TRUE;
  }

  /**
   * Get an array listing the primary key columns.
   */
  protected function getPrimaryKey() {
    $columns = array('entity_type', 'entity_id');

    // Add dynamic filter columns as components of the primary key
    foreach ($this->dynamic_filters as $dynamic_filter) {
      $columns[] = $dynamic_filter->column->getName();
    }

    return $columns;
  }

  /**
   * Delete an object from this materialized view.
   *
   * @param $entity
   *   The entity.
   */
  public function delete($entity_type, $entity_id, $run_updates = TRUE) {
    // Delete rows related to the object.

    $sql = 'DELETE FROM {' . db_escape_table($this->name) . '}
            WHERE entity_type = :entity_type AND entity_id = :entity_id';

    $num_deleted = db_query($sql, array(':entity_type' => $entity_type, ':entity_id' => $entity_id))->rowCount();

    // Apply updates to other items affected by the deletion.
    if ($run_updates) {
      $this->update($entity_type, $entity_id, TRUE);
    }

    return $num_deleted;
  }

}