<?php
/**
 * @file
 * This file contains the MVNodeColumn class
 */
class MVNodeColumn extends MVColumn {
  protected $column_name;
  public function __construct($column_name) {
    $this->column_name = $column_name;
  }

  public function getValue($entity_type, $entity_id) {
    $node = MVEntityCache::get('node', $entity_id);
    return $node[$this->column_name];
  }

  public function getChangeMapping($entity_type, $entity_id) {
    $changed = array();

    if ($entity_type == 'node') {
      $changed[$entity_type] = array($entity_id);
    }

    return $changed;
  }

  public function getSchema() {
    $schema = drupal_get_schema_unprocessed('node', 'node');
    $field_schema = $schema['fields'][$this->column_name];

    // Override type "serial"
    if ($field_schema['type'] == 'serial') {
      $field_schema['type'] = 'int';
    }

    return $field_schema;
  }

  public function getName() {
    return 'node_' . $this->column_name;
  }

}