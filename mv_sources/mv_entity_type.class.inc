<?php
/**
 * @file
 * This file contains the MVEntityType class
 * It is used to return the entity type of a particular field
 */
class MVEntityType extends MVColumn {

  public function getValue($entity_type, $entity_id) {
    return $entity_type;
  }

  public function getSchema() {
    $schema = drupal_get_schema_unprocessed('materialized_view', 'materialized_view_indexing');
    return $schema['fields']['entity_type'];
  }

  public function getName() {
    return 'entity_type';
  }

}