<?php
/**
 * @file
 * This file contains the MVNodeTerms class
 * Provides an MV data source for the terms associated with a node.
 */
class MVNodeTerms extends MVColumn {

  public function getValue($entity_type, $entity_id) {
    $node = node_load($entity_id);
    $tids = array();

    // Add in a "zero" tid for as a sort of NULL.
    $tids[] = 0;

    $res = db_query('SELECT tid FROM {taxonomy_index} WHERE nid = :nid', array(':nid' => $node->nid,));
    foreach ($res as $row) {
      $tids[] = $row->tid;
    }
    return $tids;
  }

  public function getChangeMapping($entity_type, $entity_id) {
    $changed = array();

    if ($entity_type == 'node') {
      // A change to a node only affects its own value.
      $changed['node'] = array($entity_id);
    } elseif ($entity_type == 'term') {
      // TODO: Implement change propagation for terms.
    }

    return $changed;
  }

  public function getSchema() {
    $schema = drupal_get_schema_unprocessed('taxonomy', 'taxonomy_index');
    return $schema['fields']['tid'];
  }

  public function getName() {
    return 'term_tid';
  }

}
