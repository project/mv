<?php
/**
 * @file
 * This file contains the MVLastNodeActivityTimestamp class
 * Provides an MV data source for the latest editing or comment activity on a node.
 */
class MVLastNodeActivityTimestamp extends MVColumn {

  public function getValue($entity_type, $entity_id) {
    $timestamp = db_query('SELECT MAX(c.changed) FROM {comment} c WHERE c.nid = :nid', array(':nid' => $entity_id))->fetchField();
    if (!$timestamp) {
      $timestamp = db_query('SELECT n.changed FROM {node} n WHERE n.nid = :nid', array(':nid' => $entity_id))->fetchField();
    }
    return $timestamp;
  }

  public function getChangeMapping($entity_type, $entity_id) {
    $changed = array();

    if ($entity_type == 'node') {
      // A change to a node only affects its own value.
      $changed['node'] = array($entity_id);
    } elseif ($entity_type == 'comment') {
      $comment = MVEntityCache::get('comment', $entity_id, '_comment_load');

      // A change to a comment affects the value of the node it's attached to.
      $changed['node'] = array($comment['nid']);
    }

    return $changed;
  }

  public function getSchema() {
    $schema = drupal_get_schema_unprocessed('node', 'node');
    return $schema['fields']['changed'];
  }

  public function getName() {
    return 'last_node_activity';
  }

}