<?php
/**
 * @file
 * This file contains the MVEntityColumn.
 * This class enables us to use materialize entity fields
 */
class MVEntityColumn extends MVColumn {

  protected $column_name;
  protected $type;

  public function __construct($entity_type, $column_name) {
    $this->column_name = $column_name;
    $this->type = $entity_type;
  }

  public function getValue($entity_type, $entity_id) {
    $entity = MVEntityCache::get($entity_type, $entity_id);
    return $entity[$this->column_name];
  }

  public function getChangeMapping($entity_type, $entity_id) {
    $changed = array();

    if ($entity_type == $this->type) {
      $changed[$entity_type] = array($entity_id);
    }

    return $changed;
  }

  public function getSchema() {
    $entity_info = entity_get_info($this->type);
    $table = $entity_info['base table'];
    $schema = drupal_get_schema($table);
    $field_schema = $schema['fields'][$this->column_name];

    // Override type "serial"
    if ($field_schema['type'] == 'serial') {
      $field_schema['type'] = 'int';
    }

    return $field_schema;
  }

  public function getName() {
    return $this->type . '_' . $this->column_name;
  }
}
