<?php
/**
 * @file
 * This file contains the MVNodeVocabularies class
 */
class MVNodeVocabularies extends MVColumn {

  public function getValue($entity_type, $entity_id) {
    $node = node_load($entity_id);
    $vids = array();

    // Add in a "zero" vid for as a sort of NULL.
    $vids[] = 0;

    $res = db_query('SELECT DISTINCT td.vid FROM {taxonomy_index} tn INNER JOIN {taxonomy_term_data} td ON td.tid = tn.tid WHERE tn.nid = :nid', array(':nid' => $node->nid));
    foreach ($res as $row) {
      $vids[] = $row->vid;
    }
    return $vids;
  }

  public function getChangeMapping($entity_type, $entity_id) {
    $changed = array();

    if ($entity_type == 'node') {
      // A change to a node only affects its own value.
      $changed['node'] = array($entity_id);
    } elseif ($entity_type == 'term') {
      // TODO: Implement change propagation for terms.
    } elseif ($entity_type == 'vocabulary') {
      // TODO: Implement change propagation for vocabularies.
    }

    return $changed;
  }

  public function getSchema() {
    $schema = drupal_get_schema_unprocessed('taxonomy', 'taxonomy_term_data');
    return $schema['fields']['vid'];
  }

  public function getName() {
    return 'term_vid';
  }

}