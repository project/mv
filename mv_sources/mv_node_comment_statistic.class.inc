<?php
/**
 * @file
 * This file contains the MVNodeCommentStatistic class
 */
class MVNodeCommentStatistic extends MVColumn {

  protected $column_name;

  public function __construct($column_name) {
    $this->column_name = $column_name;
  }

  public function getValue($entity_type, $entity_id) {
    /**
     * @todo
     * Have to replace this also, once its clear whether it returns a array or a single row
     * And also all its calls
     */
    return db_query('SELECT ' . db_escape_table($this->column_name) . ' FROM {node_comment_statistics} WHERE nid = :nid', array(':nid' => $entity_id))->fetchField();
  }

  public function getChangeMapping($entity_type, $entity_id) {
    $changed = array();

    if ($entity_type == 'node') {
      // A change to a node only affects its own value.
      $changed['node'] = array($entity_id);
    } elseif ($entity_type == 'comment') {
      $comment = MVEntityCache::get('comment', $entity_id);

      // A change to a comment affects the value of the node it's attached to.
      $changed['node'] = array($comment['nid']);
    }

    return $changed;
  }

  public function getSchema() {
    $schema = drupal_get_schema_unprocessed('comment', 'node_comment_statistics');
    return $schema['fields'][$this->column_name];
  }

  public function getName() {
    return 'node_' . $this->column_name;
  }

}