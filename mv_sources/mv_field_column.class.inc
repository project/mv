<?php
/**
 * @file
 * This file contains the MVFieldColumn class
 */
class MVFieldColumn extends MVColumn {
  /**
   *
   * @var $column_name
   * It stores the field name, for this class.
   */
  protected $column_name;
  /**
   *
   * @var $path
   * It stores the field name and field column.
   * If the field column is not given, the key field is used, by default.
   */
  protected $path;
  /**
   *
   * @var $key
   * It is used to store the key field column.
   */
  protected $key;

  function __construct($entity_type, $path) {
    $this->column_name = (is_array($path)) ? $path[0] : $path;
    $entity_info = entity_get_info($entity_type);
    $this->key = $entity_info['entity keys']['id'];
    $this->path = $path;
  }

  public function getName() {
    return implode($this->path, "_");
  }

  public function getSchema() {
    $schema = field_info_field($this->column_name);

    if (!empty($this->key) && !is_array($this->path)) {
      // Check for the schema at the key field and return it, if found.
      if (!empty($schema['columns'][$this->key])) {
        return $schema['columns'][$this->key];
      } else {
        throw new Exception("Empty Schema found at the key field column. Please specify the field column of the field to be materialized\.");
      }
    } elseif (is_array($this->path)) {
      // If the path is a array, check for the field column provided.
      // If field column not found, check for the key field.
      // Throw an exception, if both of the above cases fail.
      if (!empty($schema['columns'][$this->path[1]])) {
        return $schema['columns'][$this->path[1]];
      } elseif (!empty($schema['columns'][$this->key])) {
        return $schema['columns'][$this->key];
      } else {
        throw new Exception("Empty Schema found. Check the field column entered and/or the key field.");
      }
    } else {
      throw new Exception('Empty Schema found. Please check the columns.');
    }
  }
  
// TODO Support multi-valued field columns

  public function getValue($entity_type, $entity_id) {
    $entity = entity_load($entity_type, array($entity_id));

    // maybe there can be a way to return all the items, would save time
    $value = field_get_items($entity_type, $entity[$entity_id], $this->column_name);

    if (!is_array($this->path) && !empty($this->key)) {
      return $value[0][$this->key];
    } elseif (is_array($this->path)) {
      if (!empty($value[0][$this->path[1]])) {
        return $value[0][$this->path[1]];
      } else {
        return $value[0][$this->key];
      }
    } else {
      return NULL;
    }
  }

  public function getChangeMapping($entity_type, $entity_id) {
    $changed = array();
    $changed[$entity_type] = array($entity_id);
    return $changed;
  }

}